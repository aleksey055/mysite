# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import get_template
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth import authenticate

from .models import Document
from .forms import DocumentForm


from .models import Document2
from .forms import DocumentForm2


from .models import Document3
from .forms import DocumentForm3



def list(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        form2 = DocumentForm2(request.POST, request.FILES)
        form2 = DocumentForm3(request.POST, request.FILES)


        if form.is_valid() and form2.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.save()


            newdoc2 = Document2(docfile2=request.FILES['docfile2'])
            newdoc2.save()

            newdoc3 = Document2(docfile2=request.FILES['docfile2'])
            newdoc3.save()


            # Redirect to the document list after POST
            return redirect('/')
    else:
        form = DocumentForm()  # A empty, unbound form
        form2 = DocumentForm()  # A empty, unbound form
        form3 = DocumentForm()  # A empty, unbound form        
    # Load documents for the list page
    documents = Document.objects.all()
    documents2 = Document2.objects.all()
    documents3 = Document3.objects.all()


    # Render list page with the documents and the form
    return render(request, 'blog/glavnaya.html', {'documents': documents, 'documents2': documents2, 'documents3': documents3})


