# -*- coding: utf-8 -*-
from django.db import models
from django.db import IntegrityError
from django.utils import timezone


class Document(models.Model):
    docfile = models.FileField(upload_to='documents/%Y/%m/%d', null=True, blank=True, verbose_name="Логотип")
    class Meta:
        verbose_name = 'Слайд 1'  
        verbose_name_plural = 'Слайд 1'



class Document2(models.Model):
    docfile2 = models.FileField(upload_to='documents/%Y/%m/%d', null=True, blank=True, verbose_name="Логотип2")
    class Meta:
        verbose_name = 'Слайд 2'  
        verbose_name_plural = 'Слайд 2'



class Document3(models.Model):
    docfile3 = models.FileField(upload_to='documents/%Y/%m/%d', null=True, blank=True, verbose_name="Логотип3")
    class Meta:
        verbose_name = 'Слайд 3'
        verbose_name_plural = 'Слайд 3'
