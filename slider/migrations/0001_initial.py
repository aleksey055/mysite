# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 15:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('docfile', models.FileField(blank=True, null=True, upload_to='documents/%Y/%m/%d', verbose_name='Логотип')),
            ],
            options={
                'verbose_name_plural': 'Слайд 1',
                'verbose_name': 'Слайд 1',
            },
        ),
        migrations.CreateModel(
            name='Document2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('docfile2', models.FileField(blank=True, null=True, upload_to='documents/%Y/%m/%d', verbose_name='Логотип2')),
            ],
            options={
                'verbose_name_plural': 'Слайд 2',
                'verbose_name': 'Слайд 2',
            },
        ),
        migrations.CreateModel(
            name='Document3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('docfile3', models.FileField(blank=True, null=True, upload_to='documents/%Y/%m/%d', verbose_name='Логотип3')),
            ],
            options={
                'verbose_name_plural': 'Слайд 3',
                'verbose_name': 'Слайд 3',
            },
        ),
    ]
