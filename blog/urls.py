# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.glavnaya, name='glavnaya'),


    url(r'^articles', views.articles, name='aricles'),
    url(r'^article/(?P<pk>[0-9]+)/$', views.article, name='article'),
    url(r'^article/addcom/(?P<pk>[0-9]+)/$', views.addcom, name='addcom'),
    url(r'^page/(\d+)/$', views.articles, name='articles'),
    url(r'^---/', views.artforms, name='artforms'),


    url(r'^contacts', views.contacts, name='contacts'),
    url(r'^sitemap.xml', views.sitemap, name='sitemap'),

]
