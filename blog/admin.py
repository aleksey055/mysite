from django.contrib import admin
from .models import Glavnaya
from .models import Post
from .models import Comments
from .models import Contacts
# Register your models here.


class ProgInline(admin.StackedInline):
    model = Comments
    extra = 1

class ProgAdmin(admin.ModelAdmin):
    inlines = [ProgInline]

admin.site.register(Post, ProgAdmin)
admin.site.register(Glavnaya)
admin.site.register(Contacts)
