# -*- coding: utf-8 -*-
from django import forms
from .models import Post, Comments
from django.forms import ModelForm

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['title', 'image']


class CommentForm(ModelForm):

    class Meta:
        model = Comments
        fields = ['com_text']
